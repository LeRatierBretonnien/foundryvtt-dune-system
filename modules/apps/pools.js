const KEY = "dune";
const SOCK_KEY = 'system.' + KEY;
export class Pools {
  constructor() {
    this.threat = 0;
    this.momentum = 0;
  }

  data() {
    return { threat: this.threat, momentum: this.momentum };
  }

  minusThreat() {
    if (this.threat > 0) {
      this.threat--;
      this.save();
    } else
      ui.notifications.error('Cannot have less than 0 Threat');
    this.tracker.render(false);
  }

  plusThreat() {
    this.threat++;
    this.save();
    this.tracker.render(false);
  }

  plusThreatPlayer() {
    this.threat++;
    game.socket.emit(SOCK_KEY, { type: "threat-increase", player: game.user.charname || game.user.name });
    this.tracker.render(false);
  }

  minusMomentum() {
    if (this.momentum > 0) {
      this.momentum--;
      this.save();
    } else
      ui.notifications.error('Cannot have less than 0 Momentum');
    this.tracker.render(false);
  }

  minusMomentumPlayer() {
    if (this.momentum > 0) {
      this.momentum--;
      game.socket.emit(SOCK_KEY, { type: "momentum-decrease", player: game.user.charname || game.user.name });
    } else
      ui.notifications.error('Cannot have less than 0 Momentum');
  }

  plusMomentum() {
    if (this.momentum < 6) {
      this.momentum++;
      this.save();
    } else
      ui.notifications.error('Too much Momentum');
    this.tracker.render(false);
  }

  save() {
    game.settings.set(KEY, "threat", this.threat);
    game.settings.set(KEY, "momentum", this.momentum);
  }

  show() {
    const template = "systems/dune/templates/apps/tracker.html";
    let me = this;
    if (!this.tracker) {
      this.tracker = new Application({ title: game.i18n.localize("DUNE.tracker.title"), template: template });
      this.tracker.getData = function () { return me.data(); };
      this.tracker.activateListeners = function (html) {
        //console.log(html);
        if (game.user.isGM) {
          html.find("#dune-threat-track-decrease").click(() => me.minusThreat());
          html.find("#dune-momentum-track-increase").click(() => me.plusMomentum());
          html.find("#dune-threat-track-increase").click(() => me.plusThreat());
          html.find("#dune-momentum-track-decrease").click(() => me.minusMomentum());
        } else {
          html.find("#dune-threat-track-decrease").addClass("disabled");
          html.find("#dune-momentum-track-increase").addClass("disabled");
          html.find("#dune-threat-track-increase").click(() => me.plusThreatPlayer());
          html.find("#dune-momentum-track-decrease").click(() => me.minusMomentumPlayer());
        }
        me.pos();
      };
    }
    this.tracker.render(true);
  }

  pos() {
    let m = $("#sidebar").position().top || 5;
    let pos = {
      top: $(window).height() - m - ((this.tracker.position || {}).height || 133),
      left: $("#sidebar").position().left - m - ((this.tracker.position || {}).width || 200)
    };
    this.tracker.setPosition(pos);
  }

  refresh() {
    this.refreshData();
    this.tracker.render(false);
  }

  refreshData() {
    this.threat = game.settings.get(KEY, "threat") || 0;
    this.momentum = game.settings.get(KEY, "momentum") || 0;
  }

  toggle() {
    if (this.tracker?._state > 0)
      this.tracker.close();
    else
      this.show();
  }

};

export const pools = new Pools();

Hooks.on("setup", function () {
  const def = { scope: "world", onChange: function () { pools.refresh() }, type: Number, default: 0 };
  game.settings.register(KEY, "threat", Object.assign(def, { name: "Threat" }));
  game.settings.register(KEY, "momentum", Object.assign(def, { name: "Momentum" }));
  game.settings.register(KEY, "trackerRefreshRate", Object.assign(def, { name: "Refresh Rate", default: 10 }));
});

Hooks.on("ready", function () {
  pools.refreshData();
  pools.show();
  if (game.user.isGM) {
    game.socket.on(SOCK_KEY, data => {
      let ct = null;
      switch (data.type) {
        case "threat-increase":
          pools.plusThreat();
          ct = game.i18n.localize("DUNE.chat.threat-increase");
          break;
        case "momentum-decrease":
          pools.minusMomentum();
          ct = game.i18n.localize("DUNE.chat.momentum-decrease");
          break;
        default:
          console.log("Unknown message: ", data);
      }
      if (ct)
        ChatMessage.create({ content: Handlebars.compile(ct)(data) });
    })
  }
});
